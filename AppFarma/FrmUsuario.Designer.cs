﻿namespace AppFarma
{
    partial class FrmUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuario));
            this.lblNomedousu = new System.Windows.Forms.Label();
            this.NomeMed = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbOK = new System.Windows.Forms.CheckBox();
            this.btnmedicamento = new System.Windows.Forms.Button();
            this.cbomedicamentos = new System.Windows.Forms.ComboBox();
            this.cbposologia = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbquant = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNomedousu
            // 
            this.lblNomedousu.AutoSize = true;
            this.lblNomedousu.BackColor = System.Drawing.Color.Transparent;
            this.lblNomedousu.Font = new System.Drawing.Font("Georgia", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomedousu.ForeColor = System.Drawing.Color.Black;
            this.lblNomedousu.Location = new System.Drawing.Point(12, 9);
            this.lblNomedousu.Name = "lblNomedousu";
            this.lblNomedousu.Size = new System.Drawing.Size(126, 25);
            this.lblNomedousu.TabIndex = 1;
            this.lblNomedousu.Text = "Olá Usuario";
            // 
            // NomeMed
            // 
            this.NomeMed.AutoSize = true;
            this.NomeMed.BackColor = System.Drawing.Color.Transparent;
            this.NomeMed.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NomeMed.Location = new System.Drawing.Point(11, 59);
            this.NomeMed.Name = "NomeMed";
            this.NomeMed.Size = new System.Drawing.Size(113, 18);
            this.NomeMed.TabIndex = 2;
            this.NomeMed.Text = "Medicamento:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "Posologia:";
            // 
            // cbOK
            // 
            this.cbOK.AutoSize = true;
            this.cbOK.BackColor = System.Drawing.Color.Transparent;
            this.cbOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOK.Location = new System.Drawing.Point(17, 214);
            this.cbOK.Name = "cbOK";
            this.cbOK.Size = new System.Drawing.Size(101, 22);
            this.cbOK.TabIndex = 10;
            this.cbOK.Text = "Já Tomei!";
            this.cbOK.UseVisualStyleBackColor = false;
            this.cbOK.CheckedChanged += new System.EventHandler(this.cbOK_CheckedChanged);
            // 
            // btnmedicamento
            // 
            this.btnmedicamento.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmedicamento.Location = new System.Drawing.Point(202, 80);
            this.btnmedicamento.Name = "btnmedicamento";
            this.btnmedicamento.Size = new System.Drawing.Size(89, 31);
            this.btnmedicamento.TabIndex = 11;
            this.btnmedicamento.Text = "Novo";
            this.btnmedicamento.UseVisualStyleBackColor = true;
            this.btnmedicamento.Click += new System.EventHandler(this.btnmedicamento_Click);
            // 
            // cbomedicamentos
            // 
            this.cbomedicamentos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbomedicamentos.FormattingEnabled = true;
            this.cbomedicamentos.Location = new System.Drawing.Point(14, 80);
            this.cbomedicamentos.Name = "cbomedicamentos";
            this.cbomedicamentos.Size = new System.Drawing.Size(134, 21);
            this.cbomedicamentos.TabIndex = 12;
            this.cbomedicamentos.SelectedIndexChanged += new System.EventHandler(this.cboProduto_SelectedIndexChanged);
            // 
            // cbposologia
            // 
            this.cbposologia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbposologia.FormattingEnabled = true;
            this.cbposologia.Items.AddRange(new object[] {
            "2/2 ",
            "4/4 ",
            "8/8",
            "1x. Dia"});
            this.cbposologia.Location = new System.Drawing.Point(14, 135);
            this.cbposologia.Name = "cbposologia";
            this.cbposologia.Size = new System.Drawing.Size(134, 21);
            this.cbposologia.TabIndex = 13;
            this.cbposologia.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(21, 271);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 42);
            this.button1.TabIndex = 15;
            this.button1.Text = "Salvar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "Quantidade:";
            // 
            // cbquant
            // 
            this.cbquant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbquant.FormattingEnabled = true;
            this.cbquant.Items.AddRange(new object[] {
            "2/2 ",
            "4/4 ",
            "8/8",
            "1x. Dia"});
            this.cbquant.Location = new System.Drawing.Point(12, 180);
            this.cbquant.Name = "cbquant";
            this.cbquant.Size = new System.Drawing.Size(134, 21);
            this.cbquant.TabIndex = 17;
            this.cbquant.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged_1);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(202, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(89, 31);
            this.button2.TabIndex = 18;
            this.button2.Text = "Logoff";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(162, 271);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(103, 42);
            this.button3.TabIndex = 19;
            this.button3.Text = "Consultar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // FrmUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(305, 326);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cbquant);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbposologia);
            this.Controls.Add(this.cbomedicamentos);
            this.Controls.Add(this.btnmedicamento);
            this.Controls.Add(this.cbOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.NomeMed);
            this.Controls.Add(this.lblNomedousu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmUsuario";
            this.Load += new System.EventHandler(this.FrmUsuario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomedousu;
        private System.Windows.Forms.Label NomeMed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbOK;
        private System.Windows.Forms.Button btnmedicamento;
        private System.Windows.Forms.ComboBox cbomedicamentos;
        private System.Windows.Forms.ComboBox cbposologia;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbquant;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}