﻿using AppFarma.Base.Classes.Cadastro;
using AppFarma.Base.Classes.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFarma
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        public void ls(string nome)
        {
            
        }
        private void FrmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnentrar_Click(object sender, EventArgs e)
        {
            try
            {
                string login = txtlogin.Text;
                string senha = txtsenha.Text;


                LoginBusiness busi = new LoginBusiness();
                bool logou = busi.logar(login, senha);
                if (logou == true)
                {
                    FrmUsuario usuario = new FrmUsuario();
                    usuario.Show();
                    this.Hide();
                }


                else
                {
                    MessageBox.Show("verifique se login e/ou senha estão corretos! | login e/ou senha não cadastrado!", "login", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
                }

            }
            catch
            {
                MessageBox.Show("verifique se login e/ou senha estão corretos! | login e/ou senha não cadastrado!", "login", MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation);
            }


        }

        private void btncadast_Click(object sender, EventArgs e)
        {
            FrmCadastro usu = new FrmCadastro();
            usu.Show();
            this.Hide();
        }
    }
}


  

   

 

