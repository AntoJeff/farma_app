﻿namespace AppFarma
{
    partial class FrmCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtpergunta = new System.Windows.Forms.TextBox();
            this.txtresposta = new System.Windows.Forms.TextBox();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.btnvoltarlogin = new System.Windows.Forms.Button();
            this.lblIdade = new System.Windows.Forms.Label();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.txtidade = new System.Windows.Forms.TextBox();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(174, 18);
            this.label4.TabIndex = 65;
            this.label4.Text = "Resposta de segurança";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(25, 181);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(175, 18);
            this.label7.TabIndex = 66;
            this.label7.Text = "Pergunta de segurança";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(25, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 18);
            this.label8.TabIndex = 67;
            this.label8.Text = "CPF";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(25, 103);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 18);
            this.label9.TabIndex = 70;
            this.label9.Text = "Nome";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(28, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 20);
            this.label13.TabIndex = 71;
            this.label13.Text = "Senha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(28, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 72;
            this.label2.Text = "Usuario";
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(28, 38);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(222, 20);
            this.txtusuario.TabIndex = 73;
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(28, 119);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(222, 20);
            this.txtnome.TabIndex = 75;
            // 
            // txtpergunta
            // 
            this.txtpergunta.Location = new System.Drawing.Point(28, 202);
            this.txtpergunta.Name = "txtpergunta";
            this.txtpergunta.Size = new System.Drawing.Size(222, 20);
            this.txtpergunta.TabIndex = 77;
            // 
            // txtresposta
            // 
            this.txtresposta.Location = new System.Drawing.Point(28, 241);
            this.txtresposta.Name = "txtresposta";
            this.txtresposta.Size = new System.Drawing.Size(220, 20);
            this.txtresposta.TabIndex = 78;
            this.txtresposta.Tag = "";
            this.txtresposta.TextChanged += new System.EventHandler(this.txtresposta_TextChanged);
            // 
            // btnsalvar
            // 
            this.btnsalvar.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalvar.Location = new System.Drawing.Point(28, 307);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(109, 43);
            this.btnsalvar.TabIndex = 79;
            this.btnsalvar.Text = "Encaminhar";
            this.btnsalvar.UseVisualStyleBackColor = true;
            this.btnsalvar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnvoltarlogin
            // 
            this.btnvoltarlogin.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvoltarlogin.Location = new System.Drawing.Point(175, 307);
            this.btnvoltarlogin.Name = "btnvoltarlogin";
            this.btnvoltarlogin.Size = new System.Drawing.Size(75, 43);
            this.btnvoltarlogin.TabIndex = 81;
            this.btnvoltarlogin.Text = "Voltar";
            this.btnvoltarlogin.UseVisualStyleBackColor = true;
            this.btnvoltarlogin.Click += new System.EventHandler(this.btnvoltarlogin_Click);
            // 
            // lblIdade
            // 
            this.lblIdade.AutoSize = true;
            this.lblIdade.BackColor = System.Drawing.Color.Transparent;
            this.lblIdade.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdade.Location = new System.Drawing.Point(27, 264);
            this.lblIdade.Name = "lblIdade";
            this.lblIdade.Size = new System.Drawing.Size(50, 18);
            this.lblIdade.TabIndex = 82;
            this.lblIdade.Text = "Idade";
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(28, 80);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(220, 20);
            this.txtsenha.TabIndex = 85;
            this.txtsenha.UseSystemPasswordChar = true;
            // 
            // txtidade
            // 
            this.txtidade.Location = new System.Drawing.Point(30, 281);
            this.txtidade.Name = "txtidade";
            this.txtidade.Size = new System.Drawing.Size(65, 20);
            this.txtidade.TabIndex = 86;
            this.txtidade.Tag = "";
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(26, 163);
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(222, 20);
            this.txtcpf.TabIndex = 87;
            // 
            // FrmCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AppFarma.Properties.Resources.farmácia;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(296, 362);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.txtidade);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.lblIdade);
            this.Controls.Add(this.btnvoltarlogin);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.txtresposta);
            this.Controls.Add(this.txtpergunta);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.txtusuario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmCadastro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FrmCadastro_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtpergunta;
        private System.Windows.Forms.TextBox txtresposta;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.Button btnvoltarlogin;
        private System.Windows.Forms.Label lblIdade;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.TextBox txtidade;
        private System.Windows.Forms.TextBox txtcpf;
    }
}