﻿using AppFarma.Base.Classes.Medicamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFarma
{
    public partial class FrmCadastromed : Form
    {
        public FrmCadastromed()
        {
            InitializeComponent();
        }

        public void btnsalvar_Click(object sender, EventArgs e)
        {
            try {

                int quantidades = Convert.ToInt32(txtquantidade.Text);

                MedicamentoDTO dto = new MedicamentoDTO();
                dto.medicamento = txtmedicamento.Text;
                dto.genericos = txtGenerico.Text;
                dto.similares = txtSimilar.Text;
                dto.tempo = dtptempo.MaxDate;
                dto.quantidade = quantidades;
                dto.princativ = txtPA.Text;
                MedicamentoBusiness busi = new MedicamentoBusiness();
                busi.Salvar(dto);
                FrmMedicamento medicamento = new FrmMedicamento();
                MessageBox.Show("ok", "Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                medicamento.Show();

                this.Hide();
            }
            catch
            {
                MessageBox.Show("Por favor, verifique se os campo estão preenchidos corretamento!", "Medicamento", MessageBoxButtons.RetryCancel, MessageBoxIcon.Information);
            }
            }

        private void mtbtempo_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }

      
    }

