﻿namespace AppFarma
{
    partial class FrmMedicamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMedicamento = new System.Windows.Forms.DataGridView();
            this.lblmedi = new System.Windows.Forms.Label();
            this.btnpesquisar = new System.Windows.Forms.Button();
            this.btnCadastrar = new System.Windows.Forms.Button();
            this.btnvoltarusu = new System.Windows.Forms.Button();
            this.txtpesquisa = new System.Windows.Forms.TextBox();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrincAtivo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedicamento)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMedicamento
            // 
            this.dgvMedicamento.BackgroundColor = System.Drawing.SystemColors.ButtonShadow;
            this.dgvMedicamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMedicamento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.PrincAtivo,
            this.Column1,
            this.Column2,
            this.quantidade});
            this.dgvMedicamento.Location = new System.Drawing.Point(10, 105);
            this.dgvMedicamento.Name = "dgvMedicamento";
            this.dgvMedicamento.Size = new System.Drawing.Size(764, 333);
            this.dgvMedicamento.TabIndex = 1;
            // 
            // lblmedi
            // 
            this.lblmedi.AutoSize = true;
            this.lblmedi.BackColor = System.Drawing.Color.Transparent;
            this.lblmedi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblmedi.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmedi.Location = new System.Drawing.Point(0, 60);
            this.lblmedi.Name = "lblmedi";
            this.lblmedi.Size = new System.Drawing.Size(115, 20);
            this.lblmedi.TabIndex = 2;
            this.lblmedi.Text = "Medicamento:";
            this.lblmedi.Click += new System.EventHandler(this.lblmedi_Click);
            // 
            // btnpesquisar
            // 
            this.btnpesquisar.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpesquisar.Location = new System.Drawing.Point(668, 50);
            this.btnpesquisar.Name = "btnpesquisar";
            this.btnpesquisar.Size = new System.Drawing.Size(106, 39);
            this.btnpesquisar.TabIndex = 5;
            this.btnpesquisar.Text = "Enviar";
            this.btnpesquisar.UseVisualStyleBackColor = true;
            this.btnpesquisar.Click += new System.EventHandler(this.btnpesquisar_Click);
            // 
            // btnCadastrar
            // 
            this.btnCadastrar.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar.Location = new System.Drawing.Point(102, 12);
            this.btnCadastrar.Name = "btnCadastrar";
            this.btnCadastrar.Size = new System.Drawing.Size(124, 30);
            this.btnCadastrar.TabIndex = 6;
            this.btnCadastrar.Text = "Cadastrar - se";
            this.btnCadastrar.UseVisualStyleBackColor = true;
            this.btnCadastrar.Click += new System.EventHandler(this.btnCadastrar_Click);
            // 
            // btnvoltarusu
            // 
            this.btnvoltarusu.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnvoltarusu.Location = new System.Drawing.Point(12, 12);
            this.btnvoltarusu.Name = "btnvoltarusu";
            this.btnvoltarusu.Size = new System.Drawing.Size(84, 30);
            this.btnvoltarusu.TabIndex = 7;
            this.btnvoltarusu.Text = "Voltar";
            this.btnvoltarusu.UseVisualStyleBackColor = true;
            this.btnvoltarusu.Click += new System.EventHandler(this.btnvoltarusu_Click);
            // 
            // txtpesquisa
            // 
            this.txtpesquisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpesquisa.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtpesquisa.Location = new System.Drawing.Point(121, 60);
            this.txtpesquisa.Name = "txtpesquisa";
            this.txtpesquisa.Size = new System.Drawing.Size(541, 20);
            this.txtpesquisa.TabIndex = 4;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "medicamento";
            this.Nome.HeaderText = "Medicamento";
            this.Nome.Name = "Nome";
            // 
            // PrincAtivo
            // 
            this.PrincAtivo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PrincAtivo.DataPropertyName = "princativ";
            this.PrincAtivo.HeaderText = "Principio Ativo";
            this.PrincAtivo.Name = "PrincAtivo";
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column1.DataPropertyName = "genericos";
            this.Column1.HeaderText = "Genericos";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column2.DataPropertyName = "similares";
            this.Column2.HeaderText = "Similares";
            this.Column2.Name = "Column2";
            // 
            // quantidade
            // 
            this.quantidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.quantidade.DataPropertyName = "quantidade";
            this.quantidade.HeaderText = "Quantidade";
            this.quantidade.Name = "quantidade";
            // 
            // FrmMedicamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AppFarma.Properties.Resources.farmácia;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(786, 450);
            this.Controls.Add(this.btnvoltarusu);
            this.Controls.Add(this.btnCadastrar);
            this.Controls.Add(this.btnpesquisar);
            this.Controls.Add(this.txtpesquisa);
            this.Controls.Add(this.lblmedi);
            this.Controls.Add(this.dgvMedicamento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmMedicamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultar Medicamentos";
            this.Load += new System.EventHandler(this.FrmMedicamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedicamento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMedicamento;
        private System.Windows.Forms.Label lblmedi;
        private System.Windows.Forms.Button btnpesquisar;
        private System.Windows.Forms.Button btnCadastrar;
        private System.Windows.Forms.Button btnvoltarusu;
        private System.Windows.Forms.TextBox txtpesquisa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrincAtivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantidade;
    }
}