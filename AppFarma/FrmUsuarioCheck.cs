﻿using AppFarma.Base.Classes.Usuario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFarma
{
    public partial class FrmUsuarioCheck : Form
    {
        public FrmUsuarioCheck()
        {
            InitializeComponent();
            
        }
        
      

        private void button1_Click(object sender, EventArgs e)
        {
            FrmUsuario usu = new FrmUsuario();
            usu.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UsuarioBusiness busi = new UsuarioBusiness();
            List<UsuarioDTO> consulta = busi.consultar(); 
            

            dgvCheck.AutoGenerateColumns = false;
            dgvCheck.DataSource = consulta;
        }

        private void FrmUsuarioCheck_Load(object sender, EventArgs e)
        {

        }

        private void lblnn_Click(object sender, EventArgs e)
        {

        }

        private void dgvCheck_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
