﻿using AppFarma.Base.Classes.Cadastro;
using AppFarma.Base.Classes.Medicamento;
using AppFarma.Base.Classes.Usuario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFarma
{
    public partial class FrmUsuario : Form
    {
        public FrmUsuario()
        {
            InitializeComponent();
            CarregarCombos();
           
            
        }

        public void CarregarCombos()
        {
            MedicamentoBusiness db = new MedicamentoBusiness();
            List<MedicamentoDTO> list =  db.Listar();

            cbomedicamentos.ValueMember = nameof(MedicamentoDTO.id_med);
            cbomedicamentos.DisplayMember = nameof(MedicamentoDTO.medicamento);
            cbquant.ValueMember = nameof(MedicamentoDTO.id_med);
            cbquant.DisplayMember = nameof(MedicamentoDTO.quantidade);

            cbomedicamentos.DataSource = list;
            cbquant.DataSource = list;
        }
  public void carregarcombo2()
        {
            MedicamentoBusiness busi = new MedicamentoBusiness();
            List<MedicamentoDTO> list = busi.Listar(); 
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnmedicamento_Click(object sender, EventArgs e)
        {
            FrmMedicamento medicamento = new FrmMedicamento();
            medicamento.Show();
            this.Hide();
        }

      
        public void LS(string nome)
        {
            lblNomedousu.Text = nome;

        }
        private void FrmUsuario_Load(object sender, EventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void cbOK_CheckedChanged(object sender, EventArgs e)
        {
            MessageBox.Show("Ok!","Salva", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        public void button1_Click(object sender, EventArgs e)
        {
            int qua = Convert.ToInt32(cbquant.Text);
           
                UsuarioDTO dto = new UsuarioDTO();
                
                dto.medicamento = cbomedicamentos.Text;
                dto.posologia = cbposologia.Text;
            dto.quantidade = qua ;
                dto.usuario = lblNomedousu.Text;

                UsuarioBusiness busi = new UsuarioBusiness();
                busi.Salvar(dto);
                FrmUsuarioCheck check = new FrmUsuarioCheck();
                check.Show();

                this.Hide();
            
           
        }

        private void comboBox1_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmLogin login = new FrmLogin();
            login.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FrmUsuarioCheck usu = new FrmUsuarioCheck();
            usu.Show();
            this.Hide();
        }
    }
}
