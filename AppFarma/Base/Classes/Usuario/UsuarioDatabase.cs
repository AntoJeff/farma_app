﻿using AppFarma.Base.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Usuario
{
    class UsuarioDatabase 
    {
        public int salvar(UsuarioDTO dto)
        {
            string script = @"INSERT INTO tb_usucheck (usuario, medicamento,quantidade,posologia) 
                                VALUES (@usuario,@medicamento,@quantidade,@posologia)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("usuario", dto.usuario));
            parms.Add(new MySqlParameter("medicamento", dto.medicamento));
            parms.Add(new MySqlParameter("quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("posologia", dto.posologia));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<UsuarioDTO> consultar()
        {
            string script = @"SELECT * FROM tb_usucheck";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter());

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<UsuarioDTO> lista = new List<UsuarioDTO>();
            while (reader.Read())
            {
                UsuarioDTO dto = new UsuarioDTO();
                dto.id_check = reader.GetInt32("id_check");
                dto.medicamento = reader.GetString("medicamento");
                dto.usuario = reader.GetString("usuario");
                dto.posologia = reader.GetString("posologia");
                dto.quantidade = reader.GetInt32("quantidade");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
