﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Usuario
{
    class UsuarioBusiness
    {  
        public int Salvar(UsuarioDTO dto)
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.salvar(dto);
        }
        public List<UsuarioDTO> consultar()
        {
            UsuarioDatabase db = new UsuarioDatabase();
            return db.consultar();
        }
    }
}
