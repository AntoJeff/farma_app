﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Usuario
{
    class UsuarioDTO
    {
        public int id_check { get; set; }
        public string usuario { get; set; }
        public string medicamento { get; set; }
        public int quantidade { get; set; }
        public string posologia { get; set; }
    }
}
