﻿using AppFarma.Base.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Cadastro
{
    class CadastroDatabase
    {
        public int Salvar(CadastroDTO dto)
        {
            string script = @"INSERT INTO tb_cadastro (usuario, senha, pergunta, resposta,idade, cpf, nome ) 
                            VALUES (@usuario,@senha,@pergunta,@resposta,@idade,@cpf,@nome)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.id_usuario));
            parms.Add(new MySqlParameter("usuario", dto.usuario));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("pergunta", dto.pergunta));
            parms.Add(new MySqlParameter("resposta", dto.resposta));
            parms.Add(new MySqlParameter("idade", dto.idade));
            parms.Add(new MySqlParameter("cpf", dto.cpf));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
