﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Cadastro
{
    class CadastroBusiness
    {
        public int salvar(CadastroDTO dto)
        {
            CadastroDatabase db = new CadastroDatabase();
            return db.Salvar(dto);
        }
    }
}
