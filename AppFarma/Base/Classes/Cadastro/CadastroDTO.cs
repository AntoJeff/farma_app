﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Cadastro
{
    class CadastroDTO
    {
        public int id_usuario { get; set; }
        public string usuario { get; set; }
        public string senha { get; set; }
        public string pergunta { get; set; }
        public string resposta { get; set; }
        public int idade { get; set; }
        public string cpf { get; set; }
        public string nome { get; set; }

    }
}
