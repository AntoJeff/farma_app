﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Login
{
    class LoginDTO
    {
        public string usuario { get; set; }
        public string senha { get; set; }
    }
}
