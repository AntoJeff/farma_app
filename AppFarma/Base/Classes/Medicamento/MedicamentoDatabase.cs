﻿using AppFarma.Base.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Medicamento
{
    class MedicamentoDatabase
    {
        public int salvar(MedicamentoDTO dto)
        {
            string script = @"INSERT INTO tb_medicamento(medicamento, princativ, genericos, similares, quantidade, tempo) 
                            VALUES (@medicamento,@princativ,@genericos,@similares,@quantidade,@tempo)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("medicamento", dto.medicamento));
            parms.Add(new MySqlParameter("princativ", dto.princativ));
            parms.Add(new MySqlParameter("genericos", dto.genericos));
            parms.Add(new MySqlParameter("similares", dto.similares));
            parms.Add(new MySqlParameter("quantidade", dto.quantidade));
            parms.Add(new MySqlParameter("tempo", dto.tempo));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        } 

        public List<MedicamentoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_medicamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<MedicamentoDTO> listar = new List<MedicamentoDTO>();
            while (reader.Read())
            {
                MedicamentoDTO dto = new MedicamentoDTO();
                dto.id_med = reader.GetInt32("id_med");
                dto.medicamento = reader.GetString("medicamento");
                dto.genericos = reader.GetString("genericos");
                dto.princativ = reader.GetString("princativ");
                dto.similares = reader.GetString("similares");               
                dto.tempo = reader.GetDateTime("tempo");
                dto.quantidade = reader.GetInt32("quantidade");
                listar.Add(dto);
            }
            reader.Close();
            return listar;
        }       
        
        public List<MedicamentoDTO> Consultar(string medicamento)
        {
            string script = @"SELECT * FROM tb_medicamento WHERE medicamento like @nm_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<MedicamentoDTO> consultar = new List<MedicamentoDTO>(); 
            while(reader.Read())
            {
                MedicamentoDTO dto = new MedicamentoDTO();
                dto.id_med = reader.GetInt32("id_med");
                dto.medicamento = reader.GetString("medicamento");
                dto.genericos = reader.GetString("genericos");
                dto.princativ = reader.GetString("princativ");
                dto.similares = reader.GetString("similares");
                dto.quantidade = reader.GetInt32("quantidade");
                dto.tempo = reader.GetDateTime("tempo");
                consultar.Add(dto);
            }
            reader.Close();

            return consultar;
        }
    }
}
