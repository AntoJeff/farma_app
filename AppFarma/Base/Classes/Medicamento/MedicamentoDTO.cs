﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Medicamento
{
    class MedicamentoDTO
    {
        public int id_med { get; set; }
        public string medicamento { get; set; }
        public string princativ { get; set; }
        public string genericos { get; set; }
        public string similares { get; set; }
        public int quantidade { get; set; }
        public DateTime tempo { get; set; }
    }
}
