﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppFarma.Base.Classes.Medicamento
{
    class MedicamentoBusiness
    {
        public int Salvar(MedicamentoDTO dto)
        {
            MedicamentoDatabase db = new MedicamentoDatabase();
            return db.salvar(dto);
        }
        public List<MedicamentoDTO> Listar()
        {
            MedicamentoDatabase db = new MedicamentoDatabase();
            return db.Listar();
        } 
        public List<MedicamentoDTO> Consultar(string medicamento)
        {
            MedicamentoDatabase db = new MedicamentoDatabase();
            return db.Consultar(medicamento);
        }
    }
}