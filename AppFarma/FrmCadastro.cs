﻿using AppFarma.Base.Classes.Cadastro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFarma
{
    public partial class FrmCadastro : Form
    {
        public FrmCadastro()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int idade = Convert.ToInt32(txtidade.Text);
                CadastroDTO dto = new CadastroDTO();
                dto.usuario = txtusuario.Text;
                dto.senha = txtsenha.Text;
                dto.nome = txtnome.Text;
                dto.pergunta = txtpergunta.Text;
                dto.resposta = txtresposta.Text;
                dto.cpf = txtcpf.Text;
                dto.idade = idade;
                CadastroBusiness busi = new CadastroBusiness();
                busi.salvar(dto);
                MessageBox.Show("ok","Cadastro",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Por favor, verifique se os campo estão preenchidos corretamento!", "Cadastro de Usuario", MessageBoxButtons.RetryCancel, MessageBoxIcon.Information);
            }
        }











        private void btnvoltarlogin_Click(object sender, EventArgs e)
        {
            FrmLogin login = new FrmLogin();
            login.Show();
            this.Hide();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtresposta_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
        public void ls(string nome)
        {
            txtnome.Text = nome;
        }
        private void FrmCadastro_Load(object sender, EventArgs e)
        {

        }
    }
}
