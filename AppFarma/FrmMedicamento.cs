﻿using AppFarma.Base.Classes.Medicamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFarma
{
    public partial class FrmMedicamento : Form
    {
        public FrmMedicamento()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            FrmCadastromed cadastromed = new FrmCadastromed();
            cadastromed.Show();
            this.Hide();
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            MedicamentoBusiness busi = new MedicamentoBusiness();
            List<MedicamentoDTO> lista = busi.Listar();

            dgvMedicamento.AutoGenerateColumns = false;
            dgvMedicamento.DataSource = lista;
            
            
        }

        private void FrmMedicamento_Load(object sender, EventArgs e)
        {

        }

        private void lblmedi_Click(object sender, EventArgs e)
        {

        }

        private void btnvoltarusu_Click(object sender, EventArgs e)
        {
            FrmUsuario frmUsuario = new FrmUsuario();
            frmUsuario.Show();
            this.Hide();
        }
    }
}
