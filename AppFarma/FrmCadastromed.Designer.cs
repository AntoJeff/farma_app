﻿namespace AppFarma
{
    partial class FrmCadastromed
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblmedicamento = new System.Windows.Forms.Label();
            this.txtmedicamento = new System.Windows.Forms.TextBox();
            this.lblquantidade = new System.Windows.Forms.Label();
            this.lblgenerico = new System.Windows.Forms.Label();
            this.lblsimilar = new System.Windows.Forms.Label();
            this.lblPA = new System.Windows.Forms.Label();
            this.txtquantidade = new System.Windows.Forms.TextBox();
            this.txtGenerico = new System.Windows.Forms.TextBox();
            this.txtSimilar = new System.Windows.Forms.TextBox();
            this.txtPA = new System.Windows.Forms.TextBox();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtptempo = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // lblmedicamento
            // 
            this.lblmedicamento.AutoSize = true;
            this.lblmedicamento.BackColor = System.Drawing.Color.Transparent;
            this.lblmedicamento.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmedicamento.Location = new System.Drawing.Point(52, 50);
            this.lblmedicamento.Name = "lblmedicamento";
            this.lblmedicamento.Size = new System.Drawing.Size(107, 18);
            this.lblmedicamento.TabIndex = 3;
            this.lblmedicamento.Text = "Medicamento";
            // 
            // txtmedicamento
            // 
            this.txtmedicamento.Location = new System.Drawing.Point(55, 71);
            this.txtmedicamento.Name = "txtmedicamento";
            this.txtmedicamento.Size = new System.Drawing.Size(214, 20);
            this.txtmedicamento.TabIndex = 4;
            // 
            // lblquantidade
            // 
            this.lblquantidade.AutoSize = true;
            this.lblquantidade.BackColor = System.Drawing.Color.Transparent;
            this.lblquantidade.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblquantidade.Location = new System.Drawing.Point(52, 94);
            this.lblquantidade.Name = "lblquantidade";
            this.lblquantidade.Size = new System.Drawing.Size(92, 18);
            this.lblquantidade.TabIndex = 9;
            this.lblquantidade.Text = "Quantidade";
            // 
            // lblgenerico
            // 
            this.lblgenerico.AutoSize = true;
            this.lblgenerico.BackColor = System.Drawing.Color.Transparent;
            this.lblgenerico.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblgenerico.Location = new System.Drawing.Point(52, 138);
            this.lblgenerico.Name = "lblgenerico";
            this.lblgenerico.Size = new System.Drawing.Size(73, 18);
            this.lblgenerico.TabIndex = 10;
            this.lblgenerico.Text = "Generico";
            // 
            // lblsimilar
            // 
            this.lblsimilar.AutoSize = true;
            this.lblsimilar.BackColor = System.Drawing.Color.Transparent;
            this.lblsimilar.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsimilar.Location = new System.Drawing.Point(52, 182);
            this.lblsimilar.Name = "lblsimilar";
            this.lblsimilar.Size = new System.Drawing.Size(60, 18);
            this.lblsimilar.TabIndex = 11;
            this.lblsimilar.Text = "Similar";
            // 
            // lblPA
            // 
            this.lblPA.AutoSize = true;
            this.lblPA.BackColor = System.Drawing.Color.Transparent;
            this.lblPA.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPA.Location = new System.Drawing.Point(52, 226);
            this.lblPA.Name = "lblPA";
            this.lblPA.Size = new System.Drawing.Size(113, 18);
            this.lblPA.TabIndex = 12;
            this.lblPA.Text = "Principio ativo";
            // 
            // txtquantidade
            // 
            this.txtquantidade.Location = new System.Drawing.Point(55, 115);
            this.txtquantidade.Name = "txtquantidade";
            this.txtquantidade.Size = new System.Drawing.Size(214, 20);
            this.txtquantidade.TabIndex = 13;
            // 
            // txtGenerico
            // 
            this.txtGenerico.Location = new System.Drawing.Point(55, 159);
            this.txtGenerico.Name = "txtGenerico";
            this.txtGenerico.Size = new System.Drawing.Size(214, 20);
            this.txtGenerico.TabIndex = 14;
            // 
            // txtSimilar
            // 
            this.txtSimilar.Location = new System.Drawing.Point(55, 203);
            this.txtSimilar.Name = "txtSimilar";
            this.txtSimilar.Size = new System.Drawing.Size(214, 20);
            this.txtSimilar.TabIndex = 15;
            // 
            // txtPA
            // 
            this.txtPA.Location = new System.Drawing.Point(55, 247);
            this.txtPA.Name = "txtPA";
            this.txtPA.Size = new System.Drawing.Size(214, 20);
            this.txtPA.TabIndex = 16;
            // 
            // btnsalvar
            // 
            this.btnsalvar.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalvar.Location = new System.Drawing.Point(78, 328);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(137, 56);
            this.btnsalvar.TabIndex = 17;
            this.btnsalvar.Text = "Salvar";
            this.btnsalvar.UseVisualStyleBackColor = true;
            this.btnsalvar.Click += new System.EventHandler(this.btnsalvar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 270);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Tempo";
            // 
            // dtptempo
            // 
            this.dtptempo.Location = new System.Drawing.Point(55, 292);
            this.dtptempo.Name = "dtptempo";
            this.dtptempo.Size = new System.Drawing.Size(200, 20);
            this.dtptempo.TabIndex = 20;
            // 
            // FrmCadastromed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::AppFarma.Properties.Resources.farmácia;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(337, 433);
            this.Controls.Add(this.dtptempo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.txtPA);
            this.Controls.Add(this.txtSimilar);
            this.Controls.Add(this.txtGenerico);
            this.Controls.Add(this.txtquantidade);
            this.Controls.Add(this.lblPA);
            this.Controls.Add(this.lblsimilar);
            this.Controls.Add(this.lblgenerico);
            this.Controls.Add(this.lblquantidade);
            this.Controls.Add(this.txtmedicamento);
            this.Controls.Add(this.lblmedicamento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrmCadastromed";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro Medicamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblmedicamento;
        private System.Windows.Forms.TextBox txtmedicamento;
        private System.Windows.Forms.Label lblquantidade;
        private System.Windows.Forms.Label lblgenerico;
        private System.Windows.Forms.Label lblsimilar;
        private System.Windows.Forms.Label lblPA;
        private System.Windows.Forms.TextBox txtquantidade;
        private System.Windows.Forms.TextBox txtGenerico;
        private System.Windows.Forms.TextBox txtSimilar;
        private System.Windows.Forms.TextBox txtPA;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtptempo;
    }
}